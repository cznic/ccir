# ccir
Package ccir translates [cc](https://modernc.org/cc) ASTs to internal representation. (Work In Progress)

Installation

    $ go get modernc.org/ccir

Documentation: [godoc.org/modernc.org/ccir](http://godoc.org/modernc.org/ccir)
